package br.com.ronaldosr.cartoes.service;

import br.com.ronaldosr.cartoes.dataprovider.model.Cartao;
import br.com.ronaldosr.cartoes.dataprovider.model.Cliente;
import br.com.ronaldosr.cartoes.dataprovider.repository.CartaoRepository;
import br.com.ronaldosr.cartoes.dataprovider.repository.ClienteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class CartaoService {

    @Autowired
    private CartaoRepository cartaoRepository;

    @Autowired
    private ClienteRepository clienteRepository;

    @Autowired
    private ClienteService clienteService;

    /**
     *
     * @param clienteId
     * @param cartao
     * @return
     */
    public Cartao cadastrarCartao(long clienteId, Cartao cartao) {
        try {
            Cliente cliente = clienteService.consultarClientePorId(clienteId);
            cartao.setCliente(cliente);
            cartao.setAtivo(false);
            return cartaoRepository.save(cartao);
        } catch (DataAccessException e) {
            throw new RuntimeException("Erro ao cadastrar o cartão: " +
                    e.getCause().getCause().getMessage(), e);
        }
    }

    /**
     * Consultar Cartao por Id
     * @param id
     * @return
     */
    public Cartao consultarCartaoPorId(long id) {
        try {
            Optional<Cartao> cartao = cartaoRepository.findById(id);
            if(cartao.isPresent()) {
                return cartao.get();
            } else {
                throw new RuntimeException("Cartão não encontrado.");
            }
        } catch (DataAccessException e) {
            throw new RuntimeException("Erro ao consultar cartão: " +
                    e.getMessage());
        }
    }

    /**
     * Atualizar a situação do Cartao
     * @param id
     * @param situacao
     * @return
     */
    public Cartao atualizarSituacaoCartao(long id, boolean situacao) {
        try {
            Cartao cartao = consultarCartaoPorId(id);
            cartao.setAtivo(situacao);
            return cartaoRepository.save(cartao);
        } catch (DataAccessException e) {
            throw new RuntimeException("Erro ao atualiza o cartão: " +
                    e.getMessage());
        }
    }
}
