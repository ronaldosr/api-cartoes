package br.com.ronaldosr.cartoes.service;

import br.com.ronaldosr.cartoes.dataprovider.model.Cartao;
import br.com.ronaldosr.cartoes.dataprovider.model.Cliente;
import br.com.ronaldosr.cartoes.dataprovider.model.Fatura;
import br.com.ronaldosr.cartoes.dataprovider.model.Pagamento;
import br.com.ronaldosr.cartoes.dataprovider.repository.CartaoRepository;
import br.com.ronaldosr.cartoes.dataprovider.repository.ClienteRepository;
import br.com.ronaldosr.cartoes.dataprovider.repository.FaturaRepository;
import br.com.ronaldosr.cartoes.dataprovider.repository.PagamentoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Service
public class FaturaService {

    @Autowired
    private ClienteRepository clienteRepository;

    @Autowired
    private CartaoRepository cartaoRepository;

    @Autowired
    private PagamentoRepository pagamentoRepository;

    @Autowired
    private FaturaRepository faturaRepository;

    @Autowired
    private CartaoService cartaoService;

    @Autowired
    private ClienteService clienteService;

    /**
     * Consultar Fatura por Id do Cliente e Id do Cartão
     * @param clienteId
     * @param cartaoId
     * @return listaPagamentos
     */
    public List<Pagamento> consultarFatura(long clienteId, long cartaoId) {
        List<Pagamento> listaPagamento = new ArrayList<>();
        try {
            if (clienteExiste(clienteId) && cartaoExiste(cartaoId)) {
                listaPagamento = pagamentoRepository.findByCartaoId(cartaoId);
            }
            return listaPagamento;
        } catch (DataAccessException e) {
            throw new RuntimeException("Erro ao consultar fatura: " +
                    e.getMessage());
        }
    }

    /**
     * Pagar Fatura utilizando Id do Cliente e Id do Cartão
     * @param clienteId
     * @param cartaoId
     * @return fatura
     */
    public Fatura pagarFatura(long clienteId, long cartaoId) {
        try {
            List<Pagamento> listaPagamento = consultarFatura(clienteId, cartaoId);
            Fatura fatura = criarFatura(listaPagamento, clienteId, cartaoId);
            if (fatura.getValorPago().compareTo(BigDecimal.ZERO) > 0) {
                excluirPagamentos(cartaoId);
                return faturaRepository.save(fatura);
            } else {
                throw new RuntimeException("Não há lançamentos para faturar.");
            }
        } catch (DataAccessException e) {
            throw new RuntimeException("Erro ao pagar fatura: " +
                    e.getMessage());
        }

    }

    /**
     * Bloquear Cartão com base no Id do Cliente e Id do Cartão
     * @param clienteId
     * @param cartaoId
     */
    public void bloquearCartao(long clienteId, long cartaoId) {
        try {
            if (clienteExiste(clienteId) && cartaoExiste(cartaoId)) {
                cartaoService.atualizarSituacaoCartao(cartaoId, false);
            }
        } catch (DataAccessException e) {
            throw new RuntimeException("Erro ao bloquear cartão: " +
                    e.getMessage());
        }
    }

    /**
     * Excluir Pagamentos a partir do Id do Cartão
     * @param cartaoId
     */
    private void excluirPagamentos(long cartaoId) {
        try {
            List<Pagamento> pagamentos = pagamentoRepository.findByCartaoId(cartaoId);
            pagamentoRepository.deleteAll(pagamentos);

        } catch (DataAccessException e) {
            throw new RuntimeException("Erro ao excluir pagamentos ao gerar fatura: " +
                    e.getMessage());
        }
    }

    /**
     * Criar Fatura
     * @param listaPagamento
     * @param clienteId
     * @param cartaoId
     * @return fatura
     */
    private Fatura criarFatura(List<Pagamento> listaPagamento, long clienteId, long cartaoId) {
        Cliente cliente = clienteService.consultarClientePorId(clienteId);
        Cartao cartao = cartaoService.consultarCartaoPorId(cartaoId);

        Fatura fatura = new Fatura();
        fatura.setCliente(cliente);
        fatura.setCartao(cartao);
        fatura.setValorPago(calcularValorFatura(listaPagamento));
        fatura.setPagoEm(LocalDate.now());
        return fatura;
    }

    /**
     * Calcula o valor da fatura sumarizazndo os pagamentos do cliente
     * @param listaPagamento
     * @return BigDecimal
     */
    private BigDecimal calcularValorFatura(List<Pagamento> listaPagamento) {
        BigDecimal valorFatura = new BigDecimal(0.00);
        for (Pagamento pagamento : listaPagamento) {
            valorFatura = valorFatura.add(pagamento.getValor());
        }
        return valorFatura;
    }

    /**
     * Verifica se um cliente existe
     * @param clienteId
     * @return boolean
     */
    private boolean clienteExiste(long clienteId) {
        return clienteRepository.findById(clienteId).isPresent();
    }

    /**
     * Verifica se um Cartão existe
     * @param cartaoId
     * @return boolean
     */
    private boolean cartaoExiste(long cartaoId) {
        return cartaoRepository.findById(cartaoId).isPresent();
    }
}
