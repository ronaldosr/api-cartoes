package br.com.ronaldosr.cartoes.service;

import br.com.ronaldosr.cartoes.dataprovider.model.Cliente;
import br.com.ronaldosr.cartoes.dataprovider.repository.ClienteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ClienteService {

    @Autowired
    private ClienteRepository clienteRepository;

    /**
     * Cadsatrar Cliente
     * @param cliente
     * @return cliente
     */
    public Cliente cadastrarCliente(Cliente cliente) {
        try {
            return clienteRepository.save(cliente);
        } catch (RuntimeException e) {
            throw new RuntimeException("Erro ao cadastrar o cliente: " +
                    e.getMessage(), e);
        }
    }

    /**
     * Consultar Cliente por Id
     * @param id
     * @return cliente
     */
    public Cliente consultarClientePorId(long id) {
        try {
            Optional<Cliente> cliente = clienteRepository.findById(id);
            if (cliente.isPresent()) {
                return cliente.get();
            } else {
                throw new RuntimeException("Cliente não cadastrado.");
            }
        } catch (RuntimeException e) {
            throw new RuntimeException("Erro ao consultar cliente com id " + id + ": " +
                    e.getMessage(), e);
        }
    }
}
