package br.com.ronaldosr.cartoes.service;

import br.com.ronaldosr.cartoes.dataprovider.model.Cartao;
import br.com.ronaldosr.cartoes.dataprovider.model.Pagamento;
import br.com.ronaldosr.cartoes.dataprovider.repository.CartaoRepository;
import br.com.ronaldosr.cartoes.dataprovider.repository.PagamentoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class PagamentoService {


    @Autowired
    private PagamentoRepository pagamentoRepository;

    @Autowired
    private CartaoRepository cartaoRepository;

    /**
     *
     * @param cartao_id
     * @param pagamento
     * @return
     */
    public Pagamento cadastrarPagamento(long cartao_id, Pagamento pagamento) {
        try {
            Optional<Cartao> cartao = cartaoRepository.findById(cartao_id);
            if (cartao.isPresent()) {
                if (cartao.get().isAtivo()) {
                    pagamento.setCartao(cartao.get());
                    return pagamentoRepository.save(pagamento);
                } else {
                    throw new RuntimeException("Cartão não está ativo.");
                }

            } else {
                throw new RuntimeException("Cartão não cadastrado.");
            }
        } catch (DataAccessException e) {
            throw new RuntimeException("Erro ao cadastrar o pagamento: " +
                    e.getCause().getCause().getMessage(), e);
        }
    }

    public List<Pagamento> consultarPagamentosPorId(long id_cartao) {
        try {
            return pagamentoRepository.findByCartaoId(id_cartao);
        } catch (DataAccessException e) {
            throw new RuntimeException("Erro ao consultar pagamento: " +
                    e.getCause().getCause().getMessage(), e);
        }
    }
}
