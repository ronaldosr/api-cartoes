package br.com.ronaldosr.cartoes.endpoint.response;

public class BloqueioCartaoResponse {

    private String status;

    public BloqueioCartaoResponse() {
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
