package br.com.ronaldosr.cartoes.endpoint.controller;

import br.com.ronaldosr.cartoes.dataprovider.model.Pagamento;
import br.com.ronaldosr.cartoes.endpoint.request.CadastroPagamentoRequest;
import br.com.ronaldosr.cartoes.endpoint.response.PagamentoResponse;
import br.com.ronaldosr.cartoes.mapper.FaturaMapper;
import br.com.ronaldosr.cartoes.mapper.PagamentoMapper;
import br.com.ronaldosr.cartoes.service.PagamentoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/pagamentos")
public class PagamentoController {

    @Autowired
    private PagamentoService pagamentoService;

    @Autowired
    private FaturaMapper faturaMapper;

    @Autowired
    private PagamentoMapper pagamentoMapper;

    /**
     * Cadastrar Pagamento
     * @param cadastroPagamentoRequest
     * @return cartaoResponse
     */
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public PagamentoResponse pagamentoResponse (@RequestBody @Valid CadastroPagamentoRequest cadastroPagamentoRequest) {
        try {
            Pagamento pagamento = pagamentoService.cadastrarPagamento(
                    cadastroPagamentoRequest.getCartao_id(),
                    pagamentoMapper.converterParaPagamento(cadastroPagamentoRequest));
            return faturaMapper.converterParaPagamentoResponse(pagamento);
        } catch (RuntimeException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

    @GetMapping("/{id_cartao}")
    public List<PagamentoResponse> consultarPagamentosPorId(@PathVariable(name = "id_cartao") long id_cartao) {
        try {
            List<PagamentoResponse> listaPagamentoResponse = new ArrayList<>();
            List<Pagamento> listaPagamento = pagamentoService.consultarPagamentosPorId(id_cartao);

            for (Pagamento pagamento : listaPagamento) {
                PagamentoResponse pagamentoResponse = new PagamentoResponse();
                pagamentoResponse.setId(pagamento.getId());
                pagamentoResponse.setCartao_id(pagamento.getCartao().getId());
                pagamentoResponse.setDescricao(pagamento.getDescricao());
                pagamentoResponse.setValor(pagamento.getValor());
                listaPagamentoResponse.add(pagamentoResponse);
            }
            return listaPagamentoResponse;

        } catch (RuntimeException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }

    }
}
