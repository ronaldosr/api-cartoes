package br.com.ronaldosr.cartoes.endpoint.controller;

import br.com.ronaldosr.cartoes.dataprovider.model.Cartao;
import br.com.ronaldosr.cartoes.endpoint.request.CadastroCartaoRequest;
import br.com.ronaldosr.cartoes.endpoint.request.SituacaoCartaoRequest;
import br.com.ronaldosr.cartoes.endpoint.response.CartaoResponse;
import br.com.ronaldosr.cartoes.mapper.CartaoMapper;
import br.com.ronaldosr.cartoes.service.CartaoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;

@RestController
@RequestMapping("/cartao")
public class CartaoController {

    @Autowired
    private CartaoService cartaoService;

    @Autowired
    private CartaoMapper cartaoMapper;

    /**
     * Cadastrar Cartao
     * @param cadastroCartaoRequest
     * @return cartaoResponse
     */
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public CartaoResponse cadastrarCartao (@RequestBody @Valid CadastroCartaoRequest cadastroCartaoRequest) {
        try {
            Cartao cartao = cartaoService.cadastrarCartao(
                                                cadastroCartaoRequest.getClienteId(),
                                                cartaoMapper.converterParaCartao(cadastroCartaoRequest));
            return cartaoMapper.converterParaCadastroCartaoResponse(cartao);
        } catch (RuntimeException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

    @PatchMapping("/{id}")
    public CartaoResponse atualizarSituacaoCartao (@PathVariable(name = "id") long id, @RequestBody @Valid SituacaoCartaoRequest situacaoCartaoRequest) {
        try {
             Cartao cartao = cartaoService.atualizarSituacaoCartao(id, situacaoCartaoRequest.isAtivo());
            return cartaoMapper.converterParaCadastroCartaoResponse(cartao);
        } catch (RuntimeException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping("/{id}")
    public CartaoResponse consultarCartaoPorId(@PathVariable(name = "id") long id) {
        try {
            Cartao cartao = cartaoService.consultarCartaoPorId(id);
            return cartaoMapper.converterParaCadastroCartaoResponse(cartao);
        } catch (RuntimeException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }

    }

}
