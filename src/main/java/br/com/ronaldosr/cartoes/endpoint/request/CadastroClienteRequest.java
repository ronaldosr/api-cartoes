package br.com.ronaldosr.cartoes.endpoint.request;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

public class CadastroClienteRequest {

    @NotEmpty(message = "Informe um nome.")
    @Size(min = 3, max = 80, message = "O nome deve conter entre 3 e 80 caracteres.")
    private String nome;

    public CadastroClienteRequest() {
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }
}
