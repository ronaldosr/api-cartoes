package br.com.ronaldosr.cartoes.endpoint.controller;

import br.com.ronaldosr.cartoes.dataprovider.model.Cliente;
import br.com.ronaldosr.cartoes.endpoint.request.CadastroClienteRequest;
import br.com.ronaldosr.cartoes.endpoint.response.ClienteResponse;
import br.com.ronaldosr.cartoes.mapper.ClienteMapper;
import br.com.ronaldosr.cartoes.service.ClienteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;

@RestController
@RequestMapping("/cliente")
public class ClienteController {

    @Autowired
    private ClienteService clienteService;

    @Autowired
    private ClienteMapper clienteMapper;

    /**
     * Cadastrar Cliente
     * @param cadastroClienteRequest
     * @return clienteResponse
     */
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public ClienteResponse cadastrarCliente(@RequestBody @Valid CadastroClienteRequest cadastroClienteRequest) {
        try {
            Cliente cliente = clienteService.cadastrarCliente(clienteMapper.converterParaCliente(cadastroClienteRequest));
            return clienteMapper.converterParaCadastroClienteResponse(cliente);
        } catch (RuntimeException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

    /**
     * Consultar Cliente por Id
     * @param id
     * @return cliente
     */
    @GetMapping("/{id}")
    public ClienteResponse consultarClientePorId(@PathVariable(name = "id") long id) {
        try {
            Cliente cliente = clienteService.consultarClientePorId(id);
            return clienteMapper.converterParaCadastroClienteResponse(cliente);
        } catch (RuntimeException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }
}
