package br.com.ronaldosr.cartoes.endpoint.controller;

import br.com.ronaldosr.cartoes.dataprovider.model.Fatura;
import br.com.ronaldosr.cartoes.dataprovider.model.Pagamento;
import br.com.ronaldosr.cartoes.endpoint.response.BloqueioCartaoResponse;
import br.com.ronaldosr.cartoes.endpoint.response.FaturaResponse;
import br.com.ronaldosr.cartoes.endpoint.response.PagamentoResponse;
import br.com.ronaldosr.cartoes.mapper.FaturaMapper;
import br.com.ronaldosr.cartoes.service.FaturaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@RestController
@RequestMapping("/fatura")
public class FaturaController {

    @Autowired
    private FaturaService faturaService;

    @Autowired
    private FaturaMapper faturaMapper;

    /**
     * Consultar a fatura por Id do Cliente e Id do Cartão
     * @param clienteId
     * @param cartaoId
     * @return
     */
    @GetMapping("/{cliente-id}/{cartao-id}")
    public List<PagamentoResponse> consultarFatura(@PathVariable(name = "cliente-id") long clienteId,
                                                   @PathVariable(name = "cartao-id") long cartaoId) {
        try {
            List<Pagamento> listaPagamento = faturaService.consultarFatura(clienteId, cartaoId);
            return faturaMapper.converterParaListaPagamentoResponse(listaPagamento);
        } catch (RuntimeException e ) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

    /**
     * Pagar fatura por Id do Cliente e Id do Cartão
     * @param clienteId
     * @param cartaoId
     * @return
     */
    @PostMapping("/{cliente-id}/{cartao-id}/pagar")
    public FaturaResponse pagarFatura(@PathVariable(name="cliente-id") long clienteId,
                                      @PathVariable(name="cartao-id") long cartaoId) {
        try {
            Fatura fatura = faturaService.pagarFatura(clienteId, cartaoId);
            return faturaMapper.converterParaComprovanteFaturaResponse(fatura);
        } catch (RuntimeException e ) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    /**
     * Bloquear o Cartão por Id do Clieeente e Id do Cartão
     * @param clienteId
     * @param cartaoId
     * @return
     */
    @PostMapping("/{cliente-id}/{cartao-id}/expirar")
    public BloqueioCartaoResponse bloquearCartao(@PathVariable(name="cliente-id") long clienteId,
                                                 @PathVariable(name="cartao-id") long cartaoId) {
        try {
            BloqueioCartaoResponse bloqueioCartaoResponse = new BloqueioCartaoResponse();
            bloqueioCartaoResponse.setStatus("ok");;
            return bloqueioCartaoResponse;
        } catch (RuntimeException e ) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }
}
