package br.com.ronaldosr.cartoes.endpoint.request;

import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.math.BigDecimal;

public class CadastroPagamentoRequest {

    @NotNull(message = "Informe o Id do cartão.")
    private long cartao_id;

    @NotEmpty(message = "Informe a descrição para o pagamento.")
    @Size(min = 3, max = 50, message = "A descrição deve conter entre 3 e 50 caracteres.")
    private String descricao;

    @NotNull(message = "Informe o valor do pagamento.")
    @DecimalMin(value = "10.00", message = "O pagamento mínimo é de R$ 10,00.")
    private BigDecimal valor;

    public CadastroPagamentoRequest() {
    }

    public long getCartao_id() {
        return cartao_id;
    }

    public void setCartao_id(long cartao_id) {
        this.cartao_id = cartao_id;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public BigDecimal getValor() {
        return valor;
    }

    public void setValor(BigDecimal valor) {
        this.valor = valor;
    }
}
