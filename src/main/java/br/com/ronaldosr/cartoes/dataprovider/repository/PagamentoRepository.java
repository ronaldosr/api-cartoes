package br.com.ronaldosr.cartoes.dataprovider.repository;

import br.com.ronaldosr.cartoes.dataprovider.model.Pagamento;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface PagamentoRepository extends JpaRepository<Pagamento, Long> {

    List<Pagamento> findByCartaoId(long id);

}
