package br.com.ronaldosr.cartoes.dataprovider.repository;

import br.com.ronaldosr.cartoes.dataprovider.model.Cliente;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ClienteRepository extends JpaRepository<Cliente, Long> {
}
