package br.com.ronaldosr.cartoes.dataprovider.repository;

import br.com.ronaldosr.cartoes.dataprovider.model.Cartao;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CartaoRepository extends JpaRepository<Cartao, Long> {
}
