package br.com.ronaldosr.cartoes.mapper;

import br.com.ronaldosr.cartoes.dataprovider.model.Cartao;
import br.com.ronaldosr.cartoes.endpoint.request.CadastroCartaoRequest;
import br.com.ronaldosr.cartoes.endpoint.response.CartaoResponse;
import org.springframework.stereotype.Component;

@Component
public class CartaoMapper {

    public Cartao converterParaCartao(CadastroCartaoRequest cadastroCartaoRequest) {
        Cartao cartao = new Cartao();
        cartao.setNumero(cadastroCartaoRequest.getNumero());
        return cartao;
    }

    public CartaoResponse converterParaCadastroCartaoResponse(Cartao cartao) {
        CartaoResponse cartaoResponse = new CartaoResponse();
        cartaoResponse.setId(cartao.getId());
        cartaoResponse.setNumero(cartao.getNumero());
        cartaoResponse.setClienteId(cartao.getCliente().getId());
        cartaoResponse.setAtivo(cartao.isAtivo());
        return cartaoResponse;
    }
}
