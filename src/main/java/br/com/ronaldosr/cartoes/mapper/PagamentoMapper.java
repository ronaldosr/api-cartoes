package br.com.ronaldosr.cartoes.mapper;

import br.com.ronaldosr.cartoes.dataprovider.model.Pagamento;
import br.com.ronaldosr.cartoes.endpoint.request.CadastroPagamentoRequest;
import org.springframework.stereotype.Component;

@Component
public class PagamentoMapper {
    public Pagamento converterParaPagamento(CadastroPagamentoRequest cadastroPagamentoRequest) {
        Pagamento pagamento = new Pagamento();
        pagamento.setDescricao(cadastroPagamentoRequest.getDescricao());
        pagamento.setValor(cadastroPagamentoRequest.getValor());
        return pagamento;
    }
}
