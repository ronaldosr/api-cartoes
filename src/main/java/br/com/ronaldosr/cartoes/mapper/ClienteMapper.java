package br.com.ronaldosr.cartoes.mapper;

import br.com.ronaldosr.cartoes.dataprovider.model.Cliente;
import br.com.ronaldosr.cartoes.endpoint.request.CadastroClienteRequest;
import br.com.ronaldosr.cartoes.endpoint.response.ClienteResponse;
import org.springframework.stereotype.Component;

@Component
public class ClienteMapper {

    public ClienteResponse converterParaCadastroClienteResponse(Cliente cliente) {
        ClienteResponse clienteResponse = new ClienteResponse();
        clienteResponse.setId(cliente.getId());
        clienteResponse.setNome(cliente.getNome());
        return clienteResponse;
    }

    public Cliente converterParaCliente(CadastroClienteRequest cadastroClienteRequest) {
        Cliente cliente = new Cliente();
        cliente.setNome(cadastroClienteRequest.getNome());
        return cliente;
    }
}
