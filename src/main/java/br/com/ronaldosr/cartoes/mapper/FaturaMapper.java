package br.com.ronaldosr.cartoes.mapper;

import br.com.ronaldosr.cartoes.dataprovider.model.Fatura;
import br.com.ronaldosr.cartoes.dataprovider.model.Pagamento;
import br.com.ronaldosr.cartoes.endpoint.response.FaturaResponse;
import br.com.ronaldosr.cartoes.endpoint.response.PagamentoResponse;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class FaturaMapper {

    public PagamentoResponse converterParaPagamentoResponse(Pagamento pagamento) {
        PagamentoResponse pagamentoResponse = new PagamentoResponse();
        pagamentoResponse.setId(pagamento.getId());
        pagamentoResponse.setCartao_id(pagamento.getCartao().getId());
        pagamentoResponse.setDescricao(pagamento.getDescricao());
        pagamentoResponse.setValor(pagamento.getValor());
        return pagamentoResponse;
    }

    public List<PagamentoResponse> converterParaListaPagamentoResponse(List<Pagamento> listaPagamento) {
        List<PagamentoResponse> listaPagamentoResponse = new ArrayList<>();
        for (Pagamento pagamento : listaPagamento) {
            PagamentoResponse pagamentoResponse = new PagamentoResponse();
            pagamentoResponse.setId(pagamento.getId());
            pagamentoResponse.setCartao_id(pagamento.getCartao().getId());
            pagamentoResponse.setDescricao(pagamento.getDescricao());
            pagamentoResponse.setValor(pagamento.getValor());
            listaPagamentoResponse.add(pagamentoResponse);
        }
        return listaPagamentoResponse;
    }

    public FaturaResponse converterParaComprovanteFaturaResponse(Fatura fatura) {
        FaturaResponse faturaResponse = new FaturaResponse();
        faturaResponse.setId(fatura.getId());
        faturaResponse.setValorPago(fatura.getValorPago());
        faturaResponse.setPagoEm(fatura.getPagoEm());
        return faturaResponse;
    }
}
